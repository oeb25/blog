import Vue from 'vue'
import Router from 'vue-router'
import Landing from './views/Landing.vue'
import Post from './views/Post.vue'

Vue.use(Router)

export const router = new Router({
	routes: [
		{
			path: '/',
			component: Landing
		},
		{
			path: '/posts/:id',
			component: Post,
		}
	]
})
