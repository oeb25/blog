# Lectio

## Indledning

Projektet om Lectio indebærer brugerundersøgelser, digitale skolemedier med fokus på deres uddannelsesmæssige kontekst. I dette projekt lå der stor fokus at udføre en brugerundersøgelse om forskellen på de to IT-systemer Lectio og ItsLearning, vi valgte selv adspurgte, repræsentative målgruppe og interview- og analyseteknik. Vi valgte hvilken metodisk form hvorved vi ville benytte og indsamle vores resultater, enten kvalitativt eller kvantitativt.

<div class="video">
  <iframe src="//slides.com/oliverboving/lectio/embed?style=light" width="576" height="420" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

Dette er slidesne til vores forundersøgelse af Lectio.

## Problemstilling

Vores opgave bestod af at undersøge diverse fordele og ulemper i brugernes oplevelse af Lectio og Itslearning. Vi havde sat vores målgruppe på elever som bruger-perspektiv på hvordan deres opfattelse og oplevelse af lectio/Itslearning var. Idéen er at sætte det op i Lectio Vs. Itslearning, for at se hvilke elementer eleverne fremhæver i enten lectio eller Itslearning, så vi kan udarbejde hvilken læringsplatform har flest positive elementer eller negative elementer. Hvor så ud fra de interview-evalueringer kan vi udarbejde problemerne som hærger brugerne, og så evt. udarbejde løsningsforslag.

## Metode og interviewteknik

Vores gruppe udførte denne undersøgelse med brug af kvalitativ metode. Teoretisk betød dette at vi ville evaluere vores svar individuelt og dynamisk i forhold til hver enkelt interviewede person, og den endelig konklusion skulle være et resultat af analyse og fortolkning på et mere personligt plan.

I praksis betyder dette at vi skulle overholde de definerede regler for valg [kvalitativ metode](http://denstoredanske.dk/Samfund,_jura_og_politik/Sociologi/Sociologisk_metodologi/kvalitative_metoder). Mere specifikt afvigelsen fra kvantitativ vægtning af fakta og numeriske værdier og større fokus på de “ikke målbare” resultater. Som konsekvens betød det at brugerundersøgelsen skulle foretages personligt, og med dedikeret interviewperson til stede som kvalitativt og dynamisk kunne bede om uddybelse eller fokus undervejs. Vi gjorde dette idet at vi mente at en evaluering på basis af en hypotetisk fortolkning af opgavens problem-analyse bedre kunne foretages med brug af kvalitativ metode.

Vi i gruppen som også er brugere af Lectio, har nemlig selv nogle tanker om hvad der er godt og dårligt ved Lectio. Det var denne information vi ville bygge vores interview op på. Det er derfor vores spørgsmål er opbygget som et semistruktureret interview.

Som nævnt egner et semistruktureret interview sig godt til dette sammenhæng da vi mente et simpelt spørgeskema med brug af kvantitativ metode ikke kunne forklare den opstillede problemstilling dybdegående. Derudover ønskede vi i gruppen at finde interviewpersonens egne vaner, meninger og kommentare om platformen uden brug af et simpelt ja/nej format. Ved dybere undersøgelse af interviewpersonens vaner kunne vi diskutere hvilke områder hvor platformen halter og hvorvidt visse brugerne har fundet deres egne smutvej uden om platformens omtrentlige intentioner.

Resultaterne af denne undersøgelse skulle opsummeres, analyseres og fortolkes til fremlæggelse for STIL og Eniga som måske endda kunne bringe dele af vores feedback og løsningsforslag videre til Macom som håndterer, distribuere og producere versioner af Lectio til offentligheden.

Der kan læses mere i dybden om brugerundersøgelsen i dets dedikerede dokument, som kan findes [her](https://docs.google.com/document/d/1DNu6buutSoOmUvM0MOhafOxZkUBCOSF0YlJZ7bb2cC4/edit?usp=sharing).

<div class="video">
  <iframe src="//slides.com/oliverboving/brugersgelse/embed?style=light" width="576" height="420" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

Dette er de slides vi anvendte til at vise vores fund til Bettina Lundgaard Hansen fra STIL, og Jeppe Kurland fra ENIGA. De kan også finde [her](http://slides.com/oliverboving/brugersgelse).

Denne opgave blev lavet i sammarbejde med Thor O'Hagan, Jens Berg Christensen og Jonas Hagel.
