# Videnskablig Poster - HT3

![poster](videnskabeligposter_poster.png)

## Budskab

Budskabet med vores videnskabelige poster er at informere vores målgruppe om elektronik laboratoriet eksistens, og hvad der foregår. Vi vil bruge projektugen omkring robotter til at vise hvad der foregår.

## Målgruppe

Den primære målgruppe er unge som går i 9.klasse og/eller 1.g gymnasie elever på htx. 9.klasserne skal vælge gymnasium, og 1.g'ere skal vælge hvilket projektfag de skal have i fremtiden. Den sekundære målgruppe er unge som går i klasser under 9.klasse, fordi de skal først tage et valg omkring videregående uddannelse på et senere tidspunkt.

## Afsender

Afsenderen er TEC. Vi har valgt denne afsender, fordi vi vil vise oplyse målgruppen omkring elektronik laboratoriet. Fordi vi repræsentere TEC, ville vores troværdighed stige hos vores målgruppe. Det visuelle design kan også bliver understøttet af TEC identitet, ved at bruge logo, hashtags, og kontaktoplysninger.

## Kommunikationssituation

Kommunikations situationen foregår ved informations oplysende steder ved målgruppens uddannelsesinstitution. F.eks. en opslagstavle ved hovedindgangen. Både den primære og sekundære målgruppe, vil være i høje koncentrationer, fordi de bruger hovedindgangen mindst 2 gange om dagen.

## Design

Designet valgt vi på baggrund af tre kriterier; balance, overblik og variation (BOV). Designet som vi valgte havde et overskueligt design, og kompositionen var i balance fordi de forskellige elementer stod i kontrast med hinanden.

Målgruppen er en samfundsgruppe som bliver bombarderet dagligt med tusindvis af reklamer og oplysningskampagner. Derfor var det et stort krav at poster designet kunne adskille sig selv fra “alle de andre reklamer”, og fange målgruppens opmærksomhed. Designet brugte blandt andet et retorisk spørgsmål hvor vi nævnte Elon Musk. Dette skaber både interesse og identifikation med vores målgruppe, fordi Elon Musk er en rollefigur for mange unge mennesker.

Designet bruger også gestaltloven omkring lukkethed til at variere designet og gruppe vigtige informationer sammen, så målgruppen hurtigt og effektivt kan navigere posterens forskelige informationer.

Farveskemaet består af grå, orange og hvid. Farvepaletten består derfor udelukkende af sekundære farver.

![princip](videnskabligposter_princip.png)

## Værktøjer

Til både moodboard, princip og endelige design brugte vi Figma, et web- og vektorbaseret grafisk program. I Figma er det muligt at oprette projekter, bestående af ét til flere artboards, altså lærreder, så man kan holde sine forskellige idéer på samme siden, uden de slås om et enkelt lærred, som set i applikationer som andre vektor baserede applikationer som Adobe Illustrator, Inkscape, Sketch og andre.

Det største sælge punkt ved Figma frem for mere velkendte grafiske applikationer, er ikke dets grafiske funktionaliteter, men dets mulighed for at flere brugere kan arbejde samtidig på ét og samme design, som populært set i andre webbaserede applikationer, herunder Google Docs mm.

## Journalistiske greb

I den første poster vi lavede, ville vi have et interview af en elev som gik på projekt faget, El-lab. Og til det valgte vi vores klassekammerat, og gruppemedlem Morten Vindberg. Hvor vi opbyggede et interview af ham, vi opstillede det med en Personbeskrivelse af Morten, med interview som fokus.

Vi valgte som tidligere 9. Klasser - 1/2g elever som vores målgruppe, og til fremsendelsen af målgruppen analyserede vi kommunikationssituationen, såsom det er beskrevet i Kommunikation/IT A bogen på side 7.2. Ved at stille lignende spørgsmål til os selv.

Vi bruger også et retorisk spørgsmål, nederst til venstre på posteren, hvor vi spørger “Vil du være den næste Elon Musk?”. Hvor vi kan tiltrække opmærksomheden på de teknologi interesserede elever, som kunne være bekendt med Elon Musk, eller dem som er inspireret af hans arbejde, og gerne vil have den samme karrierevej.

## Anvendte Illustrationer

Til vores poster anvendte vi fire forskellige illustrationer.

### Vores Baggrundsbillede

![Baggrundsbillede](videskabligposter_baggrund.jpg)

På baggrunden af størstedelen af vores plakat, ses et billede af vores robot taget i lagkage perspektiv. Illustrationen viser ikke direkte ledningerne og fumle brættet, men der hintes til det, ved at vise de forskellig farvede ledninger.

### Illustration 1. Software

![software](videskabligposter_software.png)

I vores illustration til Software, valgte vi at indsætte en del af koden, som er i robottens software. Når man skriver kode, bruger man meget samme ord igen og igen, derfor findes der skrive programmer der ændre farverne på diverse ord (commands). f.eks. i HTML når man skal lave en usorteret liste, bruger man <ul> </ul>, og når man har disse tegn repeteret mange gange, kan det godt blive uoverskueligt. Derfor, som nævnt tidligere, tog vi inspiration fra de andre programmer f.eks. Sublime text, eller Notepad+. Og lavede vores farver til koden, det almindelige tekst er hvidt, og de repeterede dele som er væsentligt til koden, gav vi specifikke farver fra vores farvepalet.

### Illustration 2. Omdrejningspunkt

![hjul](videskabligposter_hjul.png)

I øverste højre hjørne ses en sektion der omhandler robottens omdrejningspunkt, og hvordan hjulenes placeres sammen med dens tyngdepunkt, har indflydelse på hvordan robotten reagere. Kort sagt, desto tættere tyngdepunktet ligger på centeret af hjulene, desto hurtigere kan robotten rotere om sig selv.

### Illustration 3. Lyssensor

![lyssensor](videskabligposter_lyssensor.png)

Denne illustration er meget simpelt opbygget. Dels består af en videnskabelig tekst, som formidler information omkring den lyssensor som vores robot brugte. I højre side, er der en illustration af lyssensoren, så man kan se hvordan den nogenlunde ser ud. Vi har især vægtet realisme ved illustrationen, f.eks. er ledninger de korrekte farver.

Teksten handler kort sagt omkring hvad lyssensoren komponent mæssigt består af, og hvordan den virker på en forsimplet måde.

## Fejltagelser og tanker undervejs

Da vi nåede hvad vi mente var et udkast til vores endelige design og layout, gik det op for os at vores poster på mange måder ikke lignede en videnskabelig poster, men mere en reklame poster. Vi kunne godt se hvor i vores process den var gået galt, og vi måtte foretage nogle drastiske ændringer. Vi diskuterede om vi kunne inkorporere videnskabelige illustrationer eller tekstbokse, uden at miste vores reklame lignende afdelinger. Vi konstaterede at det ikke var muligt uden drastiske ændringer ved layoutet, og valgte derfor at udskifte hver eneste afdeling med et konkret teknisk område set på robotten. Der var tre afdelinger, så vi skrev en sektion hver i gruppen, hver afdeling havde både et mængde tekst, og en informations bærende illustration.

## Skitser

![skistse 1](videskabligposter_skitse1.png)
![skistse 2](videskabligposter_skitse2.png)

## Evaluering

<div class="video">
  <iframe src="https://drive.google.com/file/d/0Bz8mRY24L6anY2NFWThyUzhpWFE/preview" frameborder="0" width="640" height="480"></iframe>
</div>

I vores videnskabelige poster var der meget fokus på design fagets rolle. Vi gik igennem de tre faser, analysering, kreative fase, og tekniske fase. Vi aflusttede med en præsentation. Under arbejdet fandt vi ud af at det blev for “reklame” agtigt, og vi ændrede meget af teksten, uden af tage højde for målgruppen. Hvilket betød at efter vi blev færdig, fandt vi så ud af at vi ikke kunne få den ønskede effekt, da vi produktet fik det forkerte udstillingssted og målgruppe.
