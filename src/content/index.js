const content = [
	['Lectio', require('./lectio.md'), {}],
	['Videnskablig Poster - HT3', require('./videnskabeligposter.md'), { layout: 'image', align: 'top', image: require('./videnskabeligposter_poster.png') }],
	['Spotify', require('./spotify.md'), { layout: 'inline', content: `<div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/GY-pDQPg4K4" frameborder="0" allowfullscreen></iframe></div>` }],
]

const encrypt = i => {
	const t = btoa((i + '').concat("0000000000").slice(0, 10))

	return (t.slice(t.length / 2) + t.slice(0, t.length / 2)).replace(/=/g, 'x')
}

export default content.map(([title, content, opts = {}], i) => ({
	id: title.replace(/[\s-]+/g, '-').toLowerCase(),
	title,
	content,
	opts
})).reverse()
