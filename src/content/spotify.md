# Spotify

<div class="video">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/GY-pDQPg4K4" frameborder="0" allowfullscreen></iframe>
</div>

Udarbejdet i sammarbejde med Oscar Springborg og Frederik Holm Nielsen.

## Argumentation for medieprodukt

Da det kom til valg for vores medieprodukt, havde vi i forvejen nogle krav vi skulle overholde. Det skulle enten være: Vodcast, fotostory, screencast, læringsobjekt, PP med speak til jeres YouTube kanal + synopsis. Med dette som en baggrund, valgte vi at tage udgangspunkt i to andre videoer Oscar og Frederik havde lavet, nemlig Zulu Historie: https://www.youtube.com/watch?v=mGwF2OCjSWI og https://www.youtube.com/watch?v=I0nfPNEEMfM .

Vi ville ikke lave endnu et afsnit af Zulu Historie, da emnet vi havde ikke var en historisk begivenhed, men en aktuelt ting. Derfor kom vi på programmet: “Zulu News”. Programmet skulle have de samme komiske træk som i Zulu Historie, men med en anden stemning. Hvor i Zulu Historie foregik hoveddelen af programmet i en “hyggelig” scene, altså hjemme i varmen, så vil Zulu News foregå ude i selve nyheden.

Af denne grund er der blevet taget inspiration i tv-serien “Man vs Wild” hvor overlevelsesekspert, Bear Grylls, udøver farlige metoder for at overleve i det værste terræner: https://www.youtube.com/watch?v=rzypas8p9Ek .  Samtidig har “Man vs Wild” en bestemt lyd som bliver brugt mellem pauser og dramatiske scener, hvilket vi også har taget inspiration fra i netop vores udsendelse: https://www.youtube.com/watch?v=SJRKG-0j0u0

Der var mange kilder vi har inspiration fra udover. Helt i starten af programmet havde vi besluttet os for at have en indtroduktion til emnet i programmet. Denne intro havde vi så valgt skulle fortælles på samme måde Bubber præsenterer programmet Grillfeber: https://www.youtube.com/watch?v=yZhL-dWVsrM . Selve dækbilleder og speaken er inspireret af de fleste moderne fakta og dokumentations programmer hvor der er speak henover nogle dækbilleder af selve speakeren der laver forskellige opgaver, fx Bonderøven og Kontant. Her skal Zulu News føles som at værten holder seerne i hånden gennem det hele, mens man sammen bliver klogere på programmets emne.  

For at opsummere inspirationskilderne vil størstedelen af inspirationen ligger i “naboprogrammet” Zulu Historie hvor næsten alt inspiration til humor og opbygning kommer fra. Derefter kommer “Man vs Wild” hvor selve værtens rolle og introen kommer fra. Meget af stemningen i Zulu News kommer også fra “Man vs Wild”, hovedsageligt fornemmelsen af uvidenhed og at reportagen i programmet er meget aktuelt. Den sidste ting taget fra “Man vs Wild” er pause lyden.

## Afviklingsmanus/Storyboard

Vi har valgt ikke at skrive et specifikt manuskript til størstedelen af vores video. De dele af videoen der fokuserede på historien, og ikke på information, var uden manuskript, fordi vi følte at det ville komme til os mere naturligt, og samtalerne ville have et mindre påtvunget “flow”. Vi havde inden optagelserne lavet et storyboard, og vi havde diskuteret hvad der skulle sige hvornår. Så brugte vi ellers herfra vores kemi og fælles komiske timing til at brygge en velfungerende video.
Manus til ekspert-scene:

“Appen 'Spotify' er blevet super 'in' i løbet af de sidste par år, og har vist sig at have en stor effekt på piratkopiering af musik. Musik var ellers før det medie der blev downloadet illegalt oftest. Men der er så opstået en problematik, idet mange kunstnere ikke mener de tjener nok penge pr. sang på Spotify. Og det er ikke løgn, før hver 11. sang der bliver hørt på Spotify, taber kunstneren penge til en værdi af 1 sang på iTunes, i forhold til hvis det var købt her. Dette har gjort at mange kunstnere, herunder store navne som Beyonce og Taylor Swift, har valgt ikke at give Spotify rettigheder til deres musik. Dette har så som følge skabt En ny bølge af piratkopiering, fordi folk ikke kan få sangene via deres nu mest brugte musikafspiller"
Manus til speak:

"For hvordan kan det være at ulovlige downloads finder sted? Er problemet virkeligt så stort? Og hvordan får disse internet pirater fat i musikken?"

Det var vigtigt for os at komedie-delene af videoen var spontane og sjove, og derfor valgte vi ikke at have et manuskript til de dele. Til de dele hvor der lægges meget vægt på information ville vi gerne sørge for at alle facts var korrekte, og at det blev overført til mediet på den rigtige måde, derfor skrev vi manuskript til dem.

![skitser](spotify_skitser.png)

Vi fik lavet os et kort storyboard, så vi havde en god generel idé om hvad vi ville frem til med videoen. Det vi havde her ændrede sig en del undervejs, da vi fik nye eller andre idéer hen ad vejen.

## Klipning

Planlægning af klipningerne kom lidt hen ad vejen. Vi havde to kameraer med ud og optage, så det gav os mulighed for at have nogle kameravinkler der ellers ikke vil være mulige. Som eksempel havde vi en scene, hvor vores vært mødte en knægt ved et busstoppested. Her kunne vi have både et total og et næbillede, vekslende mellem hinanden. Her bliver totalet brugt til at sætte scenen, og give overblik over hvor man er henne. Nærbilledet, som vi bruger mange gange når vi har værten der taler til kameraet, er brugt for at få seeren så tæt på værten som muligt. Det skal virke som om han snakker til seeren, ikke til en mikrofon i kameraet. Kameramanden er i det hele taget ikke skjult i videoen. Han ses flere gange i totalbilleder, og værten giver ham et klap på skulderen i et klip. Dette gør at seeren føler sig tættere på, som om de er med i filmen.
